#include <errno.h>
#include <limits.h>
#include <sys/time.h>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

static inline void GET_TIME_MICROSECONDS(uint64_t *ptr) {
        struct timeval tv = {0};
        int res;

        res = gettimeofday(&tv, NULL);
        if (res == -1) {
                perror("gettimeofday");
                exit(1);
        }
        *ptr = tv.tv_sec * 1000000 + tv.tv_usec;
}

static inline unsigned long int_conv(char *str) {
	char *endptr;
	errno = 0;
	unsigned long val = strtoul(str, &endptr, 10);

	if (errno != 0) {
		perror("strtol");
		exit(EXIT_FAILURE);
	}

	if (endptr == str) {
		fprintf(stderr, "No digits were found\n");
		exit(EXIT_FAILURE);
	}

	if (*endptr != '\0') {
		fprintf(stderr, "Trailing characters\n");
		exit(EXIT_FAILURE);
	}

	return val;
}

int main(int argc, char *argv[]) {
	uint64_t start, end;
	uint32_t fizz = 0;
	uint32_t buzz = 0;
	uint32_t fizzbuzz = 0;
	uint32_t normal = 0;

	if (argc < 2) {
		printf("Usage: %s ITERATION\n", argv[0]);
		return 1;
	}
	uint32_t iterations = int_conv(argv[1]);

	GET_TIME_MICROSECONDS(&start);
	for (unsigned int i = 0; i <= iterations; i++) {
#if 0
		// will behave worse if the compiler does not optimize well
		if (i % 15 == 0) {
			fizzbuzz++;
		} else if (i % 3 == 0) {
			fizz++;
		} else if (i % 5 == 0) {
		buzz++;
		} else {
			normal++;
		}

#else
		// gcc
		bool mod_3 = (i % 3) == 0;
		bool mod_5 = (i % 5) == 0;

		if (mod_3) {
			if (mod_5)
				fizzbuzz++;
			else
				fizz++;
		} else if (mod_5) {
			buzz++;
		} else {
			normal++;
		}
#endif
	}
	GET_TIME_MICROSECONDS(&end);

	printf("%u/%u/%u/%u %u\n", normal, fizz, buzz, fizzbuzz, iterations);
	printf("duration: %.6f s\n", (end - start) / 1000000.0);
}
