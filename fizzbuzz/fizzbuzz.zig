const std = @import("std");

pub fn main() !void {
    var fizz: u32 = 0;
    var buzz: u32 = 0;
    var fizzbuzz: u32 = 0;
    var normal: u32 = 0;

    const iterations: u32 = try std.fmt.parseInt(u32, std.mem.sliceTo(std.os.argv[1], 0), 10);

    var timer = try std.time.Timer.start();
    for (0..(iterations + 1)) |i| {
        if (i % 15 == 0) {
            fizzbuzz += 1;
        } else if (i % 3 == 0) {
            fizz += 1;
        } else if (i % 5 == 0) {
            buzz += 1;
        } else {
            normal += 1;
        }
    }
    const duration: f64 = @floatFromInt(timer.read());

    std.debug.print("{d}/{d}/{d}/{d} {d}\n", .{ normal, fizz, buzz, fizzbuzz, iterations });
    std.debug.print("duration: {d:.6} s\n", .{duration / 1000000000.0});
}
