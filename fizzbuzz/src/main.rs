fn main() {
    let mut normal: u32 = 0;
    let mut fizz: u32 = 0;
    let mut buzz: u32 = 0;
    let mut fizzbuzz: u32 = 0;

    let arg = std::env::args().nth(1).expect("no iteration provided");
    let iterations: u32 = arg.parse::<u32>().unwrap();

    let now = std::time::Instant::now();
    for x in 1..=iterations {
        match (x % 3, x % 5) {
            (0, 0) => fizzbuzz += 1,
            (0, _) => fizz += 1,
            (_, 0) => buzz += 1,
            _ => normal += 1
        }
    }

    let duration = now.elapsed();

    print!("{}/{}/{}/{} {}\n", normal, fizz, buzz, fizzbuzz, iterations);
    print!("duration: {:.6?} s\n", duration.as_secs_f64());
}
