const std = @import("std");
const input = @import("input.zig");

pub fn main() !void {
    var buffer: [4096]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&buffer);
    const allocator = fba.allocator();

    var i: usize = 0;
    var total_length: u64 = 0;

    const iterations: u32 = try std.fmt.parseInt(u32, std.mem.sliceTo(std.os.argv[1], 0), 10);

    const num_lines: u32 = 10_000;

    // For every string we allocate a new buffer (using dupe())
    var timer = try std.time.Timer.start();
    while (i <= iterations) : (i += 1) {
        var j: u32 = 0;

        for (input.lines) |line| {
            var str = try allocator.dupe(u8, line);
            defer allocator.free(str);
            var k: u32 = 0;
            for (str) |c| {
                str[k] = std.ascii.toLower(c);
                k += 1;
            }
            total_length += str.len;
            j += 1;
            if (j == num_lines)
                break;
        }
    }
    const duration: f64 = @floatFromInt(timer.read());

    std.debug.print("{d} {d}\n", .{ total_length, num_lines * iterations });
    std.debug.print("duration: {d:.6} s\n", .{duration / 1000000000.0});
}
