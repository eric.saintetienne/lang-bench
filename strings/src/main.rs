mod input;

use crate::input::LINES;

fn main() {
    let mut total_length: u64 = 0;

    let arg = std::env::args().nth(1).expect("no iteration provided");
    let iterations: u32 = arg.parse::<u32>().unwrap();

    const NUM_LINES: u32 = 10_000;

	// For every string we allocate a new buffer (using String::from())
    let now = std::time::Instant::now();
    for _ in 0..=iterations {
        let mut k = 0;
        for line in LINES {
            let mut s = String::from(line);
            s = s.to_lowercase();
            // Got 194946081 instead of 194945478 (!?)
            // That's 603 characters off! (why?)
            total_length += s.bytes().count() as u64; // same as len()
            k += 1;
            if k == NUM_LINES {
                break;
            }
        }
    }

    let duration = now.elapsed();

    print!("{} {}\n", total_length, NUM_LINES * iterations);
    print!("duration: {:.6?} s\n", duration.as_secs_f64());
}
