const std = @import("std");
const input = @import("input.zig");

pub fn main() !void {
    var i: usize = 0;
    var total_length: u64 = 0;

    const iterations: u32 = try std.fmt.parseInt(u32, std.mem.sliceTo(std.os.argv[1], 0), 10);

    const num_lines: u32 = 10_000;

    // We use this buffer again and again
    var buf: [4096]u8 = undefined;

    var timer = try std.time.Timer.start();
    while (i <= iterations) : (i += 1) {
        var j: u32 = 0;

        for (input.lines) |line| {
            var str = buf[0..line.len];
            var k: u32 = 0;
            for (line) |c| {
                str[k] = std.ascii.toLower(c);
                k += 1;
            }
            // std.debug.print("{} {s}\n", .{ line.len, line });
            // std.debug.print("{} {s}\n", .{ k, str });
            total_length += str.len;
            j += 1;
            if (j == num_lines)
                break;
        }
    }
    const duration: f64 = @floatFromInt(timer.read());

    std.debug.print("{d} {d}\n", .{ total_length, num_lines * iterations });
    std.debug.print("duration: {d:.6} s\n", .{duration / 1000000000.0});
}
