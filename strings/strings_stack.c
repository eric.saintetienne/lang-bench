#include <errno.h>
#include <limits.h>
#include <sys/time.h>

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "input.c"

static inline void GET_TIME_MICROSECONDS(uint64_t *ptr) {
        struct timeval tv = {0};
        int res;

        res = gettimeofday(&tv, NULL);
        if (res == -1) {
                perror("gettimeofday");
                exit(1);
        }
        *ptr = tv.tv_sec * 1000000 + tv.tv_usec;
}

static inline unsigned long int_conv(char *str) {
	char *endptr;
	errno = 0;
	unsigned long val = strtoul(str, &endptr, 10);

	if (errno != 0) {
		perror("strtol");
		exit(EXIT_FAILURE);
	}

	if (endptr == str) {
		fprintf(stderr, "No digits were found\n");
		exit(EXIT_FAILURE);
	}

	if (*endptr != '\0') {
		fprintf(stderr, "Trailing characters\n");
		exit(EXIT_FAILURE);
	}

	return val;
}

int main(int argc, char *argv[]) {
	uint64_t start, end;

	if (argc < 2) {
		printf("Usage: %s ITERATION\n", argv[0]);
		return 1;
	}
	uint32_t iterations = int_conv(argv[1]);

	uint64_t total_length = 0;
	uint32_t num_lines = 10000;

	// We use this buffer again and again
	char buf[4096];

	GET_TIME_MICROSECONDS(&start);
	for (int i = 0; i <= iterations; i++) {
		for (int j = 0; j < num_lines; j++) {
			unsigned int count = 0;
			char *s = buf;

			for (const char *l = lines[j]; *l; count++) {
				//*s++ = tolower(*l++);
				char c = *l++;
				if ((c >= 'A') && (c <= 'Z'))
					c += ('a' - 'A');
				*s++ = c;
			}
			total_length += count;
		}
	}
	GET_TIME_MICROSECONDS(&end);

	printf("%lu %u\n", total_length, num_lines * iterations);
	printf("duration: %.6f s\n", (end - start) / 1000000.0);
}
